package com.rockitgaming.pokemon.server.chat.network;

/**
 * Chat command type start with 100 till 999
 */
public class ChatCommandType {

    public static final short COMMAND_SUBSCRIBE_CHANNEL = 100;
    public static final short COMMAND_INCOMING_MESSAGE = 101;
    public static final short COMMAND_SEND_MESSAGE = 102;

}
