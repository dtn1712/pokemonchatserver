package com.rockitgaming.pokemon.server.chat.domain;

import com.mongodb.MongoClient;
import com.rockitgaming.pokemon.server.chat.Constants;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class ChatDataStore {

    private Morphia morphia;
    private Datastore datastore;

    @Autowired
    public ChatDataStore(@Qualifier("noSqlDbName") String noSqlDbName,
                         @Qualifier("noSqlHost") String noSqlHost,
                         @Qualifier("noSqlPort") int noSqlPort) {
        morphia = new Morphia();
        morphia.mapPackage(Constants.APPLICATION_BASE_PACKAGE);
        datastore = morphia.createDatastore(new MongoClient(noSqlHost, noSqlPort), noSqlDbName);
        datastore.ensureIndexes();
    }

    public Datastore getDatastore() {
        return this.datastore;
    }
}
