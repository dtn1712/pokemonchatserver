package com.rockitgaming.pokemon.server.chat.domain;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;


@Component
public class ChatMessageRepository {

    @Autowired
    private ChatDataStore chatDataStore;

    public void save(ChatMessage chatMessage) {
        chatDataStore.getDatastore().save(chatMessage);
    }
    public void update(ChatMessage chatMessage) {
        if (chatMessage == null || chatMessage.getId() == null) {
            throw new InvalidParameterException("Id cannot be null");
        }
        Datastore datastore = chatDataStore.getDatastore();
        final Query<ChatMessage> query = datastore.createQuery(ChatMessage.class)
                .filter("id = ", chatMessage.getId());
        final UpdateOperations<ChatMessage> updateOperations = datastore.createUpdateOperations(ChatMessage.class);

        setIfNotNull(updateOperations, "toChannel", chatMessage.getToChannel());
        setIfNotNull(updateOperations, "fromPlayerName", chatMessage.getFromPlayerName());
        setIfNotNull(updateOperations, "messageContent", chatMessage.getMessageContent());
        setIfNotNull(updateOperations, "messageStatus",chatMessage.getMessageStatus());
        setIfNotNull(updateOperations, "lastUpdateTime",chatMessage.getLastUpdateTime());
        setIfNotNull(updateOperations, "forbiddenWords",chatMessage.getForbiddenWords());

        if (chatMessage.getForbiddenWords() != null && chatMessage.getForbiddenWords().size() > 0) {
            updateOperations.set("isBadMessage",true);
        }

        datastore.update(query, updateOperations);
    }

    private void setIfNotNull(UpdateOperations<ChatMessage> updateOperations, String key, Object value) {
        if (value != null) {
            updateOperations.set(key, value);
        }
    }
}
