package com.rockitgaming.pokemon.server.chat.network.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.pubsub.RedisPubSubListener;
import com.lambdaworks.redis.pubsub.StatefulRedisPubSubConnection;
import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;
import com.rockitgaming.pokemon.network.socket.SocketCommandType;
import com.rockitgaming.pokemon.network.socket.message.MessageFactory;
import com.rockitgaming.pokemon.network.util.MapperUtils;
import com.rockitgaming.pokemon.server.chat.ChatService;
import com.rockitgaming.pokemon.server.chat.domain.ChatMessage;
import com.rockitgaming.pokemon.server.chat.domain.ChatMessageStatus;
import com.rockitgaming.pokemon.server.chat.network.ChatCommandType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
public class SubscribeChannelRequestHandler extends BaseAuthorizeSessionRequestHandler {

    private static final Logger logger = LogManager.getLogger(SubscribeChannelRequestHandler.class);

    private StatefulRedisPubSubConnection<String, String> connection;

    @Autowired
    private ChatService chatService;

    @Autowired
    public SubscribeChannelRequestHandler(@Qualifier("redisConnection") String redisConnection, List<String> requireFields) {
        super(requireFields);
        RedisClient client = RedisClient.create(redisConnection);
        connection = client.connectPubSub();
    }

    @Override
    public void handleAuthorizedSessionRequest(Session session, Message message, Map<String, Object> messageData) {
        connection.addListener(new RedisPubSubListener<String, String>() {
            @Override
            public void message(String channel, String message) {
                try {
                    ObjectMapper objectMapper = MapperUtils.getObjectMapper();
                    ChatMessage chatMessage = objectMapper.readValue(message, ChatMessage.class);
                    chatMessage.setLastUpdateTime(new DateTime());
                    chatMessage.setMessageStatus(ChatMessageStatus.RECEIVED);

                    Message incomingMessage = MessageFactory.createMessage(ChatCommandType.COMMAND_INCOMING_MESSAGE);
                    incomingMessage.putString(Message.DATA_KEY, objectMapper.writeValueAsString(chatMessage));
                    writeMessage(session, incomingMessage);

                    chatService.saveChatData(chatMessage);
                } catch (JsonProcessingException e) {
                    logger.error("Failed to write message",e);
                } catch (IOException e) {
                    logger.error("Failed to read message", e);
                }
            }

            @Override
            public void message(String pattern, String channel, String message) {}

            @Override
            public void subscribed(String channel, long count) {}

            @Override
            public void psubscribed(String pattern, long count) {}

            @Override
            public void unsubscribed(String channel, long count) {}

            @Override
            public void punsubscribed(String pattern, long count) {}

        });
        List<String> channels = (List<String>) messageData.get("channels");
        for (String channel : channels) {
            connection.sync().subscribe(channel);
        }
        Message successfulMessage = MessageFactory.createMessage(SocketCommandType.COMMAND_SUCCESS);
        writeMessage(session, successfulMessage);

    }

}
