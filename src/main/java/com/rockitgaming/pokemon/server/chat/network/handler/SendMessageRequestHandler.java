package com.rockitgaming.pokemon.server.chat.network.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.pubsub.StatefulRedisPubSubConnection;
import com.rockitgaming.pokemon.data.model.entity.player.Player;
import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;
import com.rockitgaming.pokemon.network.socket.SocketErrorCode;
import com.rockitgaming.pokemon.network.socket.message.MessageFactory;
import com.rockitgaming.pokemon.network.util.MapperUtils;
import com.rockitgaming.pokemon.server.chat.ChatService;
import com.rockitgaming.pokemon.server.chat.domain.ChatMessage;
import com.rockitgaming.pokemon.server.chat.domain.ChatMessageStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

@Component
public class SendMessageRequestHandler extends BaseAuthorizeSessionRequestHandler{

    private static final Logger logger = LogManager.getLogger(SendMessageRequestHandler.class);

    private StatefulRedisPubSubConnection<String, String> connection;

    @Autowired
    private ChatService chatService;

    @Autowired
    public SendMessageRequestHandler(@Qualifier("redisConnection") String redisConnection, List<String> requireFields) {
        super(requireFields);
        RedisClient client = RedisClient.create(redisConnection);
        connection = client.connectPubSub();
    }

    @Override
    public void handleAuthorizedSessionRequest(Session session, Message message, Map<String, Object> messageData) {
        try {
            ObjectMapper objectMapper = MapperUtils.getObjectMapper();
            String channel = (String) messageData.get("channel");
            String messageContent = (String) messageData.get("messageContent");

            ChatMessage chatMessage = new ChatMessage();
            chatMessage.setId(new ObjectId());
            chatMessage.setFromPlayerName(((Player)session.getUser()).getName());
            chatMessage.setMessageContent(messageContent);
            chatMessage.setSendTime( new DateTime());
            chatMessage.setMessageStatus(ChatMessageStatus.SENT);
            chatMessage.setToChannel(channel);

            connection.sync().publish(channel, objectMapper.writeValueAsString(chatMessage));
            handleChatMessageData(chatMessage);
        } catch (JsonProcessingException e) {
            logger.error("Failed to handle request", e);
            writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_HANDLE_REQUEST,
                    String.format("Failed to handle request. Please try again", session.getSessionId(), session.getClientIp())));
        }
    }

    private void handleChatMessageData(ChatMessage chatMessage) {
        try {
            Future<ChatMessage> analyzedChatMessage = chatService.analyzeChatMessage(chatMessage);
            chatService.saveChatData(analyzedChatMessage.get());
        } catch (Exception e) {
            logger.error("Failed to save chat message",e);
        }
    }
}
