package com.rockitgaming.pokemon.server.chat;


public class Constants {

    public static final String APPLICATION_NAME = "PokemonChatServer";

    public static final String CONFIG_FOLDER_PATH = APPLICATION_NAME + "/src/main/config/";

    public static final String APPLICATION_CONTEXT_PATH = "spring/beanconfig/application-context.xml";

    public static final String APPLICATION_BASE_PACKAGE = "com.rockitgaming";
}
