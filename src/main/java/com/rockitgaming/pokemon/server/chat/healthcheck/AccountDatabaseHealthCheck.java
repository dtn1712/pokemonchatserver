package com.rockitgaming.pokemon.server.chat.healthcheck;

import com.yammer.metrics.core.HealthCheck;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class AccountDatabaseHealthCheck extends HealthCheck{

    private JdbcTemplate jdbcTemplate;

    public AccountDatabaseHealthCheck(DataSource dataSource) {
        super("accountDatabaseHealthCheck");
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    protected Result check() throws Exception {
        if (jdbcTemplate.queryForObject("SELECT 1", Long.class) == 1) {
            return Result.healthy();
        }
        return Result.unhealthy("Can't ping database");
    }
}
