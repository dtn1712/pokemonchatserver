package com.rockitgaming.pokemon.server.chat.healthcheck;

import com.rockitgaming.pokemon.service.CacheService;
import com.yammer.metrics.core.HealthCheck;


public class CacheHealthCheck extends HealthCheck {

    private final CacheService cacheService;

    public CacheHealthCheck(CacheService cacheService) {
        super("cacheHealthCheck");
        this.cacheService = cacheService;
    }

    @Override
    protected Result check() throws Exception {
        if (cacheService.ping()) {
            return Result.healthy();
        }
        return Result.unhealthy("Cannot connect to cache service");
    }
}
