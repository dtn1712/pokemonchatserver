package com.rockitgaming.pokemon.server.chat.task;

import com.mongodb.DuplicateKeyException;
import com.rockitgaming.pokemon.server.chat.domain.ChatMessage;
import com.rockitgaming.pokemon.server.chat.domain.ChatMessageRepository;

public class PersistChatMessageDataTask implements Runnable {

    private ChatMessage chatMessage;
    private ChatMessageRepository chatMessageRepository;

    public PersistChatMessageDataTask(ChatMessageRepository chatMessageRepository, ChatMessage chatMessage) {
        this.chatMessageRepository = chatMessageRepository;
        this.chatMessage = chatMessage;
    }

    @Override
    public void run() {
        try {
            chatMessageRepository.save(chatMessage);
        } catch (DuplicateKeyException e) {
            chatMessageRepository.update(chatMessage);
        }
    }
}
