package com.rockitgaming.pokemon.server.chat.network.handler;

import com.rockitgaming.pokemon.data.model.entity.account.Account;
import com.rockitgaming.pokemon.network.domain.Message;
import com.rockitgaming.pokemon.network.domain.Session;
import com.rockitgaming.pokemon.network.socket.SocketErrorCode;
import com.rockitgaming.pokemon.network.socket.handler.BaseValidateInputRequestHandler;
import com.rockitgaming.pokemon.network.socket.message.MessageFactory;
import com.rockitgaming.pokemon.service.AccountService;
import com.rockitgaming.pokemon.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public abstract class BaseAuthorizeSessionRequestHandler extends BaseValidateInputRequestHandler {

    @Autowired
    protected AccountService accountService;

    @Autowired
    protected PlayerService playerService;

    public BaseAuthorizeSessionRequestHandler(List<String> requireFields) {
        super(requireFields);
    }

    @Override
    public void handleRequest(Session session, Message message, Map<String, Object> messageData) {
        if (!session.isAuthenticated() || session.getUser() == null) {
            String username = (String) messageData.get("username");
            boolean isAuthorized = false;
            Account loginAccount = null;
            if (username != null) {
                loginAccount = accountService.getCurrentLoginAccount(username, session.getClientIp());
                if (loginAccount != null) {
                    isAuthorized = true;
                }
            }
            if (!isAuthorized) {
                writeMessage(session, MessageFactory.createErrorMessage(SocketErrorCode.ERROR_UNAUTHORIZED_SESSION,
                        String.format("This session %s from IP %s is not authorized", session.getSessionId(), session.getClientIp())));
                return;
            } else {
                session.setAuthenticated(true);
                session.setUser(loginAccount.getCurrentPlayer());
                session.setLoginUsername(username);
            }
        }

        handleAuthorizedSessionRequest(session, message, messageData);
    }

    public abstract void handleAuthorizedSessionRequest(Session session, Message message, Map<String, Object> messageData);

}
